FROM base/archlinux:latest
RUN pacman --noconfirm -Syu base-devel git python-six python-tempita python-ply libxml2 repo rustup nasm qemu clang moreutils ccache cpio jam
RUN rustup update nightly
RUN rustup default nightly
RUN rustup component add rust-src
RUN cargo install xargo
